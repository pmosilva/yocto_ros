#include "ros/ros.h"
#include "autopilot_msgs/PositionState.h"

int main(int argc, char *argv[]){

    ros::init(argc, argv, "autopilot");
    ros::NodeHandle n;
    ros::Rate loop_rate(10);
    ros::Publisher position_pub = n.advertise<autopilot_msgs::PositionState>("position_state", 1000);
    while (ros::ok())
    {
        autopilot_msgs::PositionState m;

        m.latitude=-111.0;
        m.longitude=123.12;
        m.altitude=100;

        position_pub.publish(m);
   
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
