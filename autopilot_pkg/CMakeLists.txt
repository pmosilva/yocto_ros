cmake_minimum_required(VERSION 2.8.3)
project(autopilot_pkg)

find_package(catkin REQUIRED COMPONENTS  autopilot_msgs  roscpp  rospy)

catkin_package(
  INCLUDE_DIRS include
  CATKIN_DEPENDS autopilot_msgs roscpp rospy
)

include_directories(
  ${catkin_INCLUDE_DIRS}
)

add_executable(autopilot_node src/autopilot_node.cpp)

add_dependencies(autopilot_node autopilot_msgs_gencpp)

 target_link_libraries(autopilot_node
   ${catkin_LIBRARIES}
 )
install(TARGETS autopilot_node
   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
